from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.project = form.cleaned_data["project"]
            task.assignee = form.cleaned_data["assignee"]
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "create_task": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def task_list(request):
    list_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": list_tasks,
    }
    return render(request, "tasks/assigned_tasks.html", context)
