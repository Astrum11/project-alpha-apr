from django import forms
from tasks.models import Task
from projects.models import Project
from django.contrib.auth.models import User


class TaskForm(forms.ModelForm):
    project = forms.ModelChoiceField(
        queryset=Project.objects.all(), required=True
    )
    assignee = forms.ModelChoiceField(
        queryset=User.objects.all(), required=True
    )

    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        )
